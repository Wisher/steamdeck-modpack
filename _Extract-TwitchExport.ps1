<##
    .SYNOPSIS
    Extracts an archive exported from Twitch into the repository.

    .DESCRIPTION
    Extract-TwitchExport will extract all files (except the `modlist.html` file) within the given ZIP archive, ready for
    committing to the repository.

    Extract-TwitchExport will replace all existing files on disk with those in the archive, without prompting.

    .PARAMETER Path
    The path to the ZIP archive to extract. Required.

    .EXAMPLE
    Extract-TwitchExport -Path 'D:\git\Onion Sage Adventures.zip'
    overrides
    manifest.json
#>

Param(
    [Parameter(Mandatory = $True)]
    [string] $Path
)

## ---------- SCRIPT LOGIC ---------- ##

Expand-Archive -Path $Path -DestinationPath '.\' -Force

<##
## Removing this to test using the scripts to directly compile exports for uploading to twitch
## `modlist.html` does not need to be version-controlled, nor is it required for importing into Twitch.
Remove-Item -Path '.\modlist.html'
#>
