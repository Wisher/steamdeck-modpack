 #!/bin/bash

## ---------- Parameter flags ---------- ##
# -p for path if prechosen, to use ./_Extract-Export.sh -p PATH
path=''

while getopts p: flags
do
    case ${flags} in
    p) path=${OPTARG};;
    esac
done

if ['' = $path]
then
    echo FilePath:
    read path
fi

## ---------- Logic ---------- ##
unzip "${path}"
