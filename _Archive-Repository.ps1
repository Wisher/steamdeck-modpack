<##
    .SYNOPSIS
    Archives the relevant files in the repository for importing into Twitch.

    .DESCRIPTION
    Archive-Repository will archive the `manifest.json`, `modlist.html` files and `overrides` folder (and all files contained within)
    into a single ZIP archive, ready for importing into the Twitch client. (or uploading to CurseForge)

    Archive-Repository will replace an existing archive on disk, without prompting.

    .PARAMETER OutFile
    The name to be given to the resulting ZIP archive (excluding the extension). Default: Onion Sage Adventures

    .EXAMPLE
    Archive-Repository -OutFile 'onion-sage-adventures'
    onion-sage-adventures.zip
#>

Param(
    [Parameter(Mandatory = $False)]
    [string] $OutFile
)

## ----------- CONSTANTS ------------ ##

## Files and folder to archive.
$CONTENTS = @(
    '.\manifest.json'
    '.\modlist.html'
    '.\overrides'       ## Folder, so all files within are automatically included.
)

## ---------- SCRIPT LOGIC ---------- ##

If ($OutFile -eq '') {
    ## No value provided for the parameter, so use default.
    $OSA = 'Steam Deck Core'
    $Version = Read-Host 'Version'
    $OutFile = "$OSA-$Version"
}
If ($Version -eq '') {
    $OutFile = "$OSA-gitlab-export"
}

$destination = ".\$OutFile.zip"

## Optimal Compression, for file uploads.
Compress-Archive -Path $CONTENTS -CompressionLevel Optimal -DestinationPath $destination -Force